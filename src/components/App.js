import React,{ useState, Suspense } from 'react';
import Home from './Home'
import FoodMenu from './FoodMenu';
import UserList from './UserList';
import { BrowserRouter as Router,Route, Switch, useHistory } from 'react-router-dom';
import routes from '../routes';
import { GlobalProvider } from "../context/Provider"
import isAuthenticated from "../utils/isAuthenticated";
import UserLeaveConfirmation from './UserLeaveConfirmation';
import Button from './button/button';

const RenderRoute = (route) => {
  const history = useHistory();

  document.title = route.title || "TrulyContacts";
  
  if (route.needsAuth && !isAuthenticated()) {
    history.push("/auth/login");
  }
  return (
    <Route
      path={route.path}
      exact
      render={(props) => <route.component {...props} />}
    ></Route>
  );
};

function App() {
  const [confirmOpen, setConfirmOpen] = useState(true);
  return (
    <GlobalProvider>
    <Button label="label" className="button-style" />
      <Router
        getUserConfirmation={(message, callback) => {
          return UserLeaveConfirmation(
            message,
            callback,
            confirmOpen,
            setConfirmOpen
          );
        }}
      >
        <Suspense fallback={<p>Loading</p>}>
          <Switch>
            {routes.map((route, index) => (
              <RenderRoute {...route} key={index} />
            ))}
          </Switch>
        </Suspense>
      </Router>
    </GlobalProvider>
  );
}

export default App;