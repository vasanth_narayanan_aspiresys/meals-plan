import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom' ;

class UserList extends React.Component {
    state = {
        users: []
    }

    componentDidMount() {
        axios.interceptors.request.use(
            config => {
                config.headers['Authorization'] = 'Bearer ' + 'axtNeGCpbj1r4uvQZ3t1rcuyonJhDlqRnnvw';
                config.headers['Content-Type'] = 'application/json';
                return config;
            },
            error => {
                Promise.reject(error)
            });

        axios.get('https://gorest.co.in/public-api/users')
          .then(res => {
              this.setState({
                  users: res.data.result
              })
          })
    }

    render() {
        return (
            <table border="1">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.users.map(user => (
                        <tr key={user.id}>
                            <td>{user.id}</td>
                            <td>{user.first_name}</td>
                            <td>{user.last_name}</td>
                            <td>{user.email}</td>
                            <td>{user.status}</td>
                            <td><Link to="/user/">View</Link> / <Link>Edit</Link></td>
                        </tr>
                    ))}
                </tbody>
            </table>
        )
    }
}

export default UserList