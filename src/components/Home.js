import React from 'react';
import { Link } from 'react-router-dom';

export default () => (
    <div>
        <Link to="/user"><button className="buttonStyle">Users</button></Link>
        <Link to="food-menu"><button className="buttonStyle">Food Items</button></Link>
    </div>
)