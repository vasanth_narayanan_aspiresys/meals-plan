import { combineReducers } from 'redux'
import foodReducer from '../foodSlice'
import calenderReducer from '../calenderSlice'

export default combineReducers({
  food: foodReducer,
  calendar: calenderReducer
})
