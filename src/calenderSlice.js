import { createSlice } from '@reduxjs/toolkit';

const initialCalendarState = {
    sunday: {
      breakfast: null,
      lunch: null,
      dinner: null,
    },
    monday: {
      breakfast: null,
      lunch: null,
      dinner: null,
    },
    tuesday: {
      breakfast: null,
      lunch: null,
      dinner: null,
    },
    wednesday: {
      breakfast: null,
      lunch: null,
      dinner: null,
    },
    thursday: {
      breakfast: null,
      lunch: null,
      dinner: null,
    },
    friday: {
      breakfast: null,
      lunch: null,
      dinner: null,
    },
    saturday: {
      breakfast: null,
      lunch: null,
      dinner: null,
    },
  }

const calenderSlice = createSlice({
    name: 'calendar',
    initialState: initialCalendarState,
    reducers: {
      addRecipe(state, action) {
        const { day, recipe, meal } = action.payload
        state[day][meal] = recipe.label
      },
      removeFromCalendar(state, action) {
        const { recipe } = action.payload
        state.push({ [recipe.label]: recipe});
      }
   }
});

export const { addRecipe, removeFromCalendar } = calenderSlice.actions;

export default calenderSlice.reducer