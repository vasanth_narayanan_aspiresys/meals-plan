import { createSlice } from '@reduxjs/toolkit';

const foodSlice = createSlice({
    name: 'food',
    initialState:  {},
    reducers: {
      addFood(state, action) {
        const { recipe } = action.payload;
        state[recipe.label] = recipe;
      }
   }
});

export const { addFood } = foodSlice.actions;

export default foodSlice.reducer