export default [
  {
    text: "Hungary (+36)",
    value: "+36",
    flag: "hu",
    key: "hu",
  },
  {
    text: "Iceland (+354)",
    value: "+354",
    flag: "is",
    key: "is",
  },
  {
    text: "India (+91)",
    value: "+91",
    flag: "in",
    key: "in",
  },
  {
    text: "Indian Ocean Territory (+246)",
    value: "+246",
    flag: "io",
    key: "io",
  }
];
